#!/usr/bin/env bash
set -euo pipefail

rofi -theme obsidian -modi combi,ssh -combi-modi window,drun -show combi
