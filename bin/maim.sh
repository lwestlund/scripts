#!/usr/bin/env bash
set -euo pipefail

case "$1" in
    copy-selection)
        maim -s | xclip -selection clipboard -t image/png ;;
    copy-active)
        maim -i $(xdotool getactivewindow) | xclip -selection clipboard -t image/png ;;
    save)
        maim ~/Pictures/screenshot_$(date +%Y%m%d)_$(date +%H%M%S).png
esac
